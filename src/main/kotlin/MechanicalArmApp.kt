class MechanicalArm {
    var openAngle:Double = 0.0
    var altitude:Double = 0.0
    var turnedOn:Boolean = false

    fun toggle(){
        this.turnedOn = !this.turnedOn
        printState()
    }

    fun updateAngle(openAngle: Int){
        if(this.turnedOn) this.openAngle += openAngle
        if(this.openAngle < 0.0) this.openAngle = 0.0
        else if(this.openAngle > 360.0) this.openAngle = 360.0
        printState()
    }

    fun updateAltitude(altitude: Int){
        if(this.turnedOn) this.altitude += altitude
        if(this.altitude < 0.0) this.altitude = 0.0
        else if(this.altitude > 30.0) this.altitude = 30.0
        printState()
    }

    fun printState(){
        println("MechanicalArm{openAngle=${this.openAngle},altitude=${this.altitude},turnedOn=${this.turnedOn}}")
    }
}

fun main(){
    val mechanicalArm = MechanicalArm()
    mechanicalArm.toggle()
    mechanicalArm.updateAltitude(3)
    mechanicalArm.updateAngle(180)
    mechanicalArm.updateAltitude(-3)
    mechanicalArm.updateAngle(-180)
    mechanicalArm.updateAltitude(3)
    mechanicalArm.toggle()
}