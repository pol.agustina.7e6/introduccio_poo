import java.util.Scanner

fun main(){
    val escaner = Scanner(System.`in`)
    val elements = escaner.nextLine().toInt()
    var preuTotal = 0.0
    for(i in 1..elements){
        val elementSeparat = escaner.nextLine().split(' ')
        if(elementSeparat[0]=="Taulell"){
            val taulell = Taulell(elementSeparat[1].toDouble(),elementSeparat[2].toDouble(),elementSeparat[3].toDouble())
            preuTotal+=taulell.preuUnitari
        }else{
            val llisto = Llisto(elementSeparat[1].toDouble(),elementSeparat[2].toDouble())
            preuTotal+=llisto.preuUnitari
        }
    }
    println("El preu total és: $preuTotal €")
}

class Taulell(
    var preuUnitari: Double,
    val llargada: Double,
    val amplada:Double
){
    init {
        val mida = this.llargada*this.amplada
        this.preuUnitari*=mida
    }
}

class Llisto(
    var preuUnitari:Double,
    val llargada:Double
){
    init {
        this.preuUnitari *= this.llargada
    }
}