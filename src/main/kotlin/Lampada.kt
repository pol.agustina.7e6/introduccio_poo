const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN,ANSI_GREEN)

class Lampada(val id:Int){
    var color = 1
    var intensitat = 1
    var offColor = 1
    var on = false

    fun turnOn(){
        if(this.offColor in colors.indices){
            this.color = this.offColor
            this.intensitat=1
        }
        this.on = true
        mostrarEstats()
    }

    fun turnOff(){
        if(this.color > 0){
            this.offColor = this.color
            this.color=0
            this.intensitat=0
        }
        this.on = false
        mostrarEstats()
    }

    fun changeColor(){
        if(this.on){
            if (this.color < colors.lastIndex) this.color++
            else if(this.color == colors.lastIndex) this.color=1
        }
        mostrarEstats()
    }

    fun intensity(){
        if(this.on){
            if(this.intensitat==5) this.intensitat = 1
            else this.intensitat++
        }
        mostrarEstats()
    }

    fun mostrarEstats(){
        println("Lampada: ${this.id} | Color: ${colors[this.color]}    $ANSI_RESET - Intensitat ${this.intensitat}")
    }

}

fun main(){
    val lampada1 = Lampada(1)
    val lampada2 = Lampada(2)

    //Primera
    lampada1.turnOn()
    for(i in 1..3) lampada1.changeColor()
    for(i in 1..4) lampada1.intensity()

    //Segona
    lampada2.turnOn()
    for(i in 1..2) lampada2.changeColor()
    for(i in 1..4) lampada2.intensity()
    lampada2.turnOff()
    lampada2.changeColor()
    lampada2.turnOn()
    lampada2.changeColor()
    for(i in 1..4) lampada2.intensity()



}

