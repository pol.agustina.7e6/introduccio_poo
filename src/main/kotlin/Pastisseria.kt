fun main (){
    val croissant = Pasta("croissant",1.50,70.5,15)
    val ensaimada = Pasta("ensaimada",1.40,75.2,12)
    val donut = Pasta("donut",2.00,50.0,55)
    val pastes = listOf(croissant,ensaimada,donut)
    for (pasta in pastes) println("Nom: ${pasta.nom}, Preu: ${pasta.preu} €, Pes: ${pasta.pes} g, Calories: ${pasta.calories} kcal")
    val aigua = Beguda("aigua",1.00,false)
    val cafe_tallat = Beguda("café tallat",1.35,false)
    val te_vermell = Beguda("té vermell",1.50,false)
    val cola = Beguda("cola",1.65,true)
    val begudes = listOf(aigua,cafe_tallat,te_vermell,cola)
    for (beguda in begudes) println("Nom: ${beguda.nom}, Preu: ${beguda.preu} €")

}

class Pasta(
    val nom:String,
    val preu:Double,
    val pes:Double,
    val calories:Int
)

class Beguda(
    val nom:String,
    var preu:Double,
    val ensucrada:Boolean
){
    init {
        if(this.ensucrada) this.preu*=1.1
    }
}